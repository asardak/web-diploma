function calculateResult(a, b, c, d, e, f, g, h, i) {
    return (a * b * c * d * e * f * g * h * i).toFixed(3);
}

function calculateSPCH(schr) {
    return (0.7 * schr).toFixed(3);
}

/*/////////////////////////////////////////////////  расчет чистовой подачи (Sчс) //////////////////////// */
function calculateSchist(rB, Rz) {
    return Math.sqrt(rB * Rz / 125).toFixed(3);
}


/*/////////////////////////////////////////////////  расчет скорости резания на черновом проходе (υчр) //////////////////////// */

function calculateVChern(CHvт, CHkнв, CHkт, CHkφ, CHkп, CHkип, CHkз, CHkс, schr) {
    return (CHvт * ((10 * schr) ** (-0.26)) * (3 ** (-0.16)) * CHkнв * CHkт * CHkφ * CHkп * CHkип * CHkз * CHkс).toFixed(3);
}

/*/////////////////////////////////////////////////  расчет скорости резания на получистовом проходе (υпч) //////////////////////// */

function calculateVPOLCHIST(PCHυт, PCHkнв, PCHkт, PCHkφ, PCHkп, PCHkип, PCHkз, PCHkс, spch) {
    return (PCHυт * ((10 * spch) ** (-0.26)) * (0.8 ** (-0.16)) * PCHkнв * PCHkт * PCHkφ * PCHkп * PCHkип * PCHkз * PCHkс).toFixed(3);
}

/*/////////////////////////////////////////////////  расчет скорости резания на чистовом проходе (υчс) //////////////////////// */

function calculateVCHIST(CHISTυт, CHISTkнв, CHISTkт, CHISTkφ, CHISTkп, CHISTkип, CHISTkз, CHISTkс, Schist) {
    return (CHISTυт * ((10 * Schist) ** (-0.26)) * (0.2 ** (-0.16)) * CHISTkнв * CHISTkт * CHISTkφ * CHISTkп * CHISTkип * CHISTkз * CHISTkс).toFixed(3);
}

/*/////////////////////////////////////////////////  расчет силы резания на черновом проходе (Pz) //////////////////////// */

function calculatesila(Ks, FI, k1, k2, k3, schr) {
    return (3.0 * schr * Ks * ((0.4 / Math.sin(FI)) ** 0.29) * ((k1 / 2) ** 0.07) * ((k2 / 2) ** 0.07) * k3).toFixed(3);
}

/*/////////////////////////////////////////////////  расчет мощности резания на черновом проходе (N) //////////////////////// */

function calculateN(sila, VChern) {
    return ((sila * VChern) / (1020 * 60)).toFixed(3);
}

/*/////////////////////////////////////////////////  Перерасчет частоты вращения Vчрн //////////////////////// */

function calculatePVCH(VChern) {
    return ((1000 * VChern) / (3.14 * 40)).toFixed(3);
}

/*/////////////////////////////////////////////////  Перерасчет частоты вращения Vполчистовой //////////////////////// */

function calculatePVpol(VPOLCHIST) {
    return ((1000 * VPOLCHIST) / (3.14 * 40)).toFixed(3);
}

/*/////////////////////////////////////////////////  Перерасчет частоты вращения Vchist //////////////////////// */

function calculatePVchist(VCHIST) {
    return ((1000 * VCHIST) / (3.14 * 40)).toFixed(3);
}

function Clik() {
    document.querySelector("1").innerHTML = "777";
}






(function() {
    if (document.querySelector(".butt_back,.butt_next,.back,.next")) {
        document.querySelectorAll(".butt_back,.butt_next,.back,.next").forEach(function(button) {
            button.addEventListener('click', function(event) {
                event.preventDefault();

                document.location.href = event.target.attributes.href.value + window.location.search;
            });
        });
    }

    if (document.querySelectorAll(".insert_value")) {
        document.querySelectorAll(".insert_value").forEach(function(button) {
            button.addEventListener('click', function(event) {
                event.preventDefault();

                input = document.querySelector('.value');
                input.value = event.target.innerText;

                var values = Object.fromEntries(new URLSearchParams(window.location.search));
                values[input.name] = input.value;

                document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
            });
        });
    }

    if (document.querySelectorAll(".value")) {
        document.querySelectorAll(".value").forEach(function(input) {
            params = new URLSearchParams(window.location.search);



            if (params.getAll(input.name).length > 0) {
                input.value = params.getAll(input.name).slice(-1)[0];
            }
        });
    }

    if (document.querySelector("#RachetChern")) {
        document.querySelector("#RachetChern").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            a = parseFloat(values["a"]);
            b = parseFloat(values["b"]);
            c = parseFloat(values["c"]);
            d = parseFloat(values["d"]);
            e = parseFloat(values["e"]);
            f = parseFloat(values["f"]);
            g = parseFloat(values["g"]);
            h = parseFloat(values["h"]);
            i = parseFloat(values["i"]);

            result = calculateResult(a, b, c, d, e, f, g, h, i);


            input = document.querySelector('.value');
            values[input.name] = result;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    if (document.querySelector("#RachetPch")) {
        document.querySelector("#RachetPch").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            schr = parseFloat(values["schr"]);
            spch = calculateSPCH(schr);

            input = document.querySelector('.value');
            values[input.name] = spch;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }
    /*/////////////////////////////////////////////////  расчет чистовой подачи (Sчс) //////////////////////// */
    if (document.querySelector("#RachetChist")) {
        document.querySelector("#RachetChist").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            rB = parseFloat(values["rB"]);
            Rz = parseFloat(values["Rz"]);


            Schist = calculateSchist(rB, Rz);

            input = document.querySelector('.value');
            values[input.name] = Schist;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*/////////////////////////////////////////////////  расчет скорости резания на черновом проходе (υчр) //////////////////////// */
    if (document.querySelector("#RachetSkorRezChern")) {
        document.querySelector("#RachetSkorRezChern").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            CHvт = parseFloat(values["CHvт"]);
            CHkнв = parseFloat(values["CHkнв"]);
            CHkт = parseFloat(values["CHkт"]);
            CHkφ = parseFloat(values["CHkφ"]);
            CHkп = parseFloat(values["CHkп"]);
            CHkип = parseFloat(values["CHkип"]);
            CHkз = parseFloat(values["CHkз"]);
            CHkс = parseFloat(values["CHkс"]);
            schr = parseFloat(values["schr"]);



            VChern = calculateVChern(CHvт, CHkнв, CHkт, CHkφ, CHkп, CHkип, CHkз, CHkс, schr);

            input = document.querySelector('.value');
            values[input.name] = VChern;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
            document.querySelector('.value').innerHTML = VChern;
        });
    }


    /*/////////////////////////////////////////////////  расчет скорости резания на получистовом проходе (υпч) //////////////////////// */
    if (document.querySelector("#RachetSkorRezPOLCHIST")) {
        document.querySelector("#RachetSkorRezPOLCHIST").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            PCHυт = parseFloat(values["PCHυт"]);
            PCHkнв = parseFloat(values["PCHkнв"]);
            PCHkт = parseFloat(values["PCHkт"]);
            PCHkφ = parseFloat(values["PCHkφ"]);
            PCHkп = parseFloat(values["PCHkп"]);
            PCHkип = parseFloat(values["PCHkип"]);
            PCHkз = parseFloat(values["PCHkз"]);
            PCHkс = parseFloat(values["PCHkс"]);
            spch = parseFloat(values["spch"]);





            VPOLCHIST = calculateVPOLCHIST(PCHυт, PCHkнв, PCHkт, PCHkφ, PCHkп, PCHkип, PCHkз, PCHkс, spch);

            input = document.querySelector('.value');
            values[input.name] = VPOLCHIST;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();

        });
    }


    /*/////////////////////////////////////////////////  расчет скорости резания на чистовом проходе (υчс) //////////////////////// */
    if (document.querySelector("#RachetSkorRezCHIST")) {
        document.querySelector("#RachetSkorRezCHIST").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            CHISTυт = parseFloat(values["CHISTυт"]);
            CHISTkнв = parseFloat(values["CHISTkнв"]);
            CHISTkт = parseFloat(values["CHISTkт"]);
            CHISTkφ = parseFloat(values["CHISTkφ"]);
            CHISTkп = parseFloat(values["CHISTkп"]);
            CHISTkип = parseFloat(values["CHISTkип"]);
            CHISTkз = parseFloat(values["CHISTkз"]);
            CHISTkс = parseFloat(values["CHISTkс"]);
            Schist = parseFloat(values["Schist"]);

            VCHIST = calculateVCHIST(CHISTυт, CHISTkнв, CHISTkт, CHISTkφ, CHISTkп, CHISTkип, CHISTkз, CHISTkс, Schist);

            input = document.querySelector('.value');
            values[input.name] = VCHIST;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*/////////////////////////////////////////////////  расчет силы резания на черновом проходе (Pz) //////////////////////// */
    if (document.querySelector("#Rachetsila")) {
        document.querySelector("#Rachetsila").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            Ks = parseFloat(values["Ks"]);
            FI = parseFloat(values["FI"]);
            k1 = parseFloat(values["k1"]);
            k2 = parseFloat(values["k2"]);
            k3 = parseFloat(values["k3"]);
            schr = parseFloat(values["schr"]);




            sila = calculatesila(Ks, FI, k1, k2, k3, schr);

            input = document.querySelector('.value');
            values[input.name] = sila;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*/////////////////////////////////////////////////  расчет мощности резания на черновом проходе (N) //////////////////////// */
    if (document.querySelector("#RachetN")) {
        document.querySelector("#RachetN").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));

            sila = parseFloat(values["sila"]);
            VChern = parseFloat(values["VChern"]);
            N = calculateN(sila, VChern);

            input = document.querySelector('.value');
            values[input.name] = N;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }



    /*/////////////////////////////////////////////////  Перерасчет частоты вращения Vчрн //////////////////////// */
    if (document.querySelector("#RachetPVchern")) {
        document.querySelector("#RachetPVchern").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));


            VChern = parseFloat(values["VChern"]);
            PVCH = calculatePVCH(VChern);

            input = document.querySelector('.value');
            values[input.name] = PVCH;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*/////////////////////////////////////////////////  Перерасчет частоты вращения Vполучист //////////////////////// */
    if (document.querySelector("#RachetPVpolchist")) {
        document.querySelector("#RachetPVpolchist").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));


            VPOLCHIST = parseFloat(values["VPOLCHIST"]);
            PVpol = calculatePVpol(VPOLCHIST);

            input = document.querySelector('.value');
            values[input.name] = PVpol;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*/////////////////////////////////////////////////  Перерасчет частоты вращения Vchist //////////////////////// */
    if (document.querySelector("#RachetPVchist")) {
        document.querySelector("#RachetPVchist").addEventListener('click', function(event) {
            event.preventDefault();

            var values = Object.fromEntries(new URLSearchParams(window.location.search));


            VCHIST = parseFloat(values["VCHIST"]);
            PVchist = calculatePVchist(VCHIST);

            input = document.querySelector('.value');
            values[input.name] = PVchist;

            document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
        });
    }

    /*************************************** Сохранение и вывод значения ********************************************/
    if (document.querySelectorAll(".set_val")) {
        document.querySelectorAll(".set_val").forEach(function(button) {
            button.addEventListener('click', function(event) {
                event.preventDefault();

                var values = Object.fromEntries(new URLSearchParams(window.location.search));
                values[button.name] = button.innerText;

                document.location.href = document.location.pathname + '?' + (new URLSearchParams(values)).toString();
            });
        });
    }

    if (document.querySelectorAll(".get_val")) {
        document.querySelectorAll(".get_val").forEach(function(element) {
            params = new URLSearchParams(window.location.search);

            if (params.getAll(element.getAttribute('name')).length > 0) {
                element.innerText = params.getAll(element.getAttribute('name')).slice(-1)[0];
            }
        });
    }

})();

/*/////////////////////////////////////////////////  кнопка "печать" //////////////////////// */

// let Knp = document.querySelector('button');
// Knp.onclick = function() {
//     window.print();
// };